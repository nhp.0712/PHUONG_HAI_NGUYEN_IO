//----------------------------------- Header -----------------------------------
write("Io Language − Phuong Hai Nguyen")

// Function#1: Write a function that prints the string “Hello,world.”
//----------------------------------- Function 1 -----------------------------------
"\n\nFunction 1" println
"----------" println
STR := "Hello, world."
hello := method(STR, STR println)
hello(STR)

// Function#2: Write a function that accepts 2 parameters: A string and a substring to ﬁnd in that string. 
// The function should ﬁnd the index of substring in string and print that index.
//----------------------------------- Function 2 -----------------------------------
"\nFunction 2" println
"----------" println
subSTR := method(str, substr, str containsSeq(substr))
indexsubSTR := method(str, substr, str size - substr size)

"Finding ''Me'' in ''SearchMe''" println
if (subSTR("SearchMe","Me")) then (
	"SearchMe contains Me" println "Me is found at index " print indexsubSTR("SearchMe","Me") println
	) else ("SearchMe does not contains Me" println)

"\nFinding ''They'' in ''SearchMe''" println
if (subSTR("SearchMe","They")) then (
	"SearchMe contains They" println "They is found at index " print indexsubSTR("SearchMe","They") println
	) else ("SearchMe does not contains They" println)

// Function#3: Write a function that accepts a single parameter – A list/array – and performs 3 actions: (1) Sort the array and print 
// the values, (2) Sort the array in reverse order and print the values, and (3) Prints the value of each array/list item that has a 
// ODD index. The array passed into this function must contain at least 100 randomly generated values. 
// Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) asyou ﬁnd appropriate.
//----------------------------------- Function 3 -----------------------------------
"\nFunction 3" println
"----------" println
arr := List clone
100 repeat (arr append (Random value(300) floor))
IOSort := method(arr, 
	arr := arr sort
	"Sorted: " print
	arr foreach (i, value, if (i == arr size - 1) then (write(value)) else (write(value, ", ")))
	"\n\nReverse Sorted: " print
	arr := arr reverse
	arr foreach (i, value, if (i == arr size - 1) then (write(value)) else (write(value, ", ")))
	"\n\nOdd only: " print
	arr := arr reverse
	arr foreach (i, value, if (i isOdd) then (if (i == arr size - 1) then (write(value)) else (write(value, ", "))))
)
IOSort(arr)

// Function#4: Generate a list/array of 100 random values. Sort the list using a SelectionSort
//----------------------------------- Function 4 -----------------------------------
"\n\nFunction 4" println
"----------" println
arr := List clone
100 repeat (arr append (Random value(300) floor))
"Unsorted: " print
arr foreach (i, value, if (i == arr size - 1) then (write(value)) else (write(value, ", ")))
SelectionSort := method(arr, 
	for(i, 0, arr size - 1,
		for(j, i+1, arr size - 1 , 
			if (arr at(i) > arr at(j)) then (arr swapIndices(j,i))
		)
	)
	"\n\nSelection Sort: "print
	arr foreach (i, value, if (i == arr size - 1) then (write(value)) else (write(value, ", ")))
)

SelectionSort(arr)


// Function#5: Implement a function to calculate the Fibonacci Numbers iteratively. Attemptto calculate as many Fibonacci numbers as possible. 
// The Fibonacci Series is deﬁned below with F1=F2=1 and F0=0. Fn = Fn−1+Fn−2 
//----------------------------------- Function 5 -----------------------------------
"\n\nFunction 5" println
"----------" println
fib := method( next,
    if((next == 1) or (next == 2)) then (return( 1 ))
	fibRecur := method( next, cur, a, b,
		if(next == cur) then (return(a + b))
		return(fibRecur(next, cur + 1, b, a + b))
	)
	return(fibRecur( next, 3, 1, 1 ))
)

for(i, 1, 20,
	"Fib(" print
	i print
	") := " print
	fib(i) println
)
	
// Function#6: Implement a function to calculate the Fibonacci Series iteratively.
//----------------------------------- Function 6 -----------------------------------
"\nFunction 6" println
"----------" println
fibIter := method(
	a := 1
    b := 0
  	for(i, 0, doMessage(call message argAt(0)) - 1, c := a + b; a := b; b := c)
)

for(i, 1, 20,
	"Fib(" print
	i print
	") := " print
	fibIter(i) println
)

// Function#7: Implement a function to perform matrix multiplication. The function should take two parameters – Matrix A and Matrix B – and
// return the resultant matrix – Matrix C. Test by generating a variety of random matrices that are ﬁlled with random decimal values between 
// 0 and 100. These ﬁles may be in separate ﬁles, classes, etc. and should follow the paradigms of the current language. 
//----------------------------------- Function 7 -----------------------------------
"\nFunction 7" println
"----------" println
MATRIX_SIZE := 5
mA := List clone
for(i, 0, MATRIX_SIZE - 1,
	tempList := List clone
    mA append(
    	for(i, 0, MATRIX_SIZE - 1,
    		tempList append(Random value(300) floor)
        )
    )
)

mB := List clone
for(i, 0, MATRIX_SIZE - 1,
	tempList := List clone
    mB append(
    	for(i, 0, MATRIX_SIZE - 1,
    		tempList append(Random value(300) floor)
        )
    )
)

mC := List clone
for(i, 0, MATRIX_SIZE - 1,
	tempList := List clone
    mC append(
    	for(i, 0, MATRIX_SIZE - 1,
    		tempList append(0)
        )
    )
)

MatrixMulti := method(mA, mB,
	for(i, 0, mA size-1,
		tempList := List clone
        for(j, 0, mB size -1,
        	sum := 0
            for(k, 0, mB size-1,
            	sum = sum + mA at(i) at(k) * mB at(k) at(j)
            )
        tempList append(sum)
    	)
    	mC atPut(i, tempList)
	)
		return mC
)
mC := MatrixMulti(mA,mB)

"Matrix A: " println
for(i, 0, mA size-1,
	for(k, 0, mA size-1,
		mA at(i) at(k) print
		" " print
	)
	"" println
)

"\nMatrix B: " println
for(i, 0, mB size-1,
	for(k, 0, mB size-1,
		mB at(i) at(k) print
		" " print
	)
	"" println
)

"\nMatrix C := Matrix A * Matrix B" println
for(i, 0, mC size-1,
	for(k, 0, mC size-1,
		mC at(i) at(k) print
		" " print
	)
	"" println
)

// Function#8: The Hamming distance between two numbers is the number of bits that must be changed to make the representations of the two numbers 
// equal. For instance, the word “Rob” and “Bob” differ by the ﬁrst letter, so their Hamming distance is 1. Strings like “Rob” and “Robby” differ
// by 2 characters, so the Hamming distance is 2. Write a function that accepts one parameter – a ﬁlename. This ﬁle will contain two strings on 
// each line, with each string being separated by a “ ”. Your function should read each line, calculate the Hamming distance between the two values, 
// and then write the line back to a new ﬁle in the format Value1:Value2:HammingDistance. It is up to you to generate a test ﬁle for this function. 
// The output ﬁle should insert the string _Output into the original ﬁlename. If the input ﬁle is “HammingData.csv” then the output ﬁle would be HammingData_Output.csv.
//----------------------------------- Function 8 -----------------------------------
"\nFunction 8" println
"----------" println
File_Name := "index.txt"
HamDis := method(File_Name,
	fIn := File with(File_Name)

	fIn openForReading
	lines := List clone
	lines := fIn readLines
	ham := List clone
	indexOfSpace := 0
	val := List clone

	for(i,0, lines size - 1,
		for (j,0, lines at(i) size - 1,
			if(lines at(i) at(j) asCharacter == " ") then (
				indexOfSpace := j
				val append (lines at (i) exSlice(0,indexOfSpace))
				val append (lines at (i) exSlice(indexOfSpace + 1, lines at(i) size))
			)
		)
	)

	for(k,0, val size - 1,
		if(k isEven) then(
			count := 0
			for(i,0, if(val at(k) size - val at(k+1) size < 0) then (return val at (k) size - 1) else (return val at(k+1) size - 1),
				if(val at(k) at(i) asCharacter != val at(k+1) at (i) asCharacter) then (count := count + 1)
			)
			ham append(count)
			writeln(val at (k), ":", val at(k+1), ":", ham at(k))
		)else(ham append(0))
	)			  
	fIn close

	fOut := File with ("index_Output.txt")
	fOut openForAppending
	for(k,0, val size - 1,
		if(k isEven) then(
	fOut write(val at (k), ":", val at(k+1), ":", ham at(k) asString, "\n")
		)
	)
	fOut close
)
HamDis (File_Name)

// Function #9: Create a csvReader class that is capable of opening, reading, and accessing the data from any given CSV ﬁle. The csvReader class should use a csVRow object to
//store the data from each row in the CSV ﬁle. This function should contain code that demonstrates the ability of your class to read, iterate, and display the contents of a CSV ﬁle.
//----------------------------------- Function 9 -----------------------------------
"\nFunction 9" println
"----------" println
csvReader := Object clone
csvFile := "index.csv"
csvReader := method(csvFile,
	f := File with(csvFile)
	f openForReading
	lines := List clone
	f foreachLine(i, lines,
		indexOfSpace := List clone
		csvRow := List clone
		k := 0
		indexOfSpace append(-1)
		for (j,0, lines  size - 1,
			if(lines at(j) asCharacter == ",") then (
				indexOfSpace append (j)
				csvRow append (lines exSlice(indexOfSpace at (k)+1,j))
				k := k+1
			)
			if(j == lines size - 1) then (
				indexOfSpace append (j)
				csvRow append (lines exSlice(indexOfSpace at (k)+1,j+1))
				k := k+1
			)
		)
		if(i==0) then(write("Header: ")) else(write("\nRow ", i,":  "))
		for(k, 0, csvRow size-1,
			csvRow at(k) print
			if(i==0) then("         " print) else("      " print)
		)
	)
)
csvReader(csvFile)

// Task #10: Implement the equivalent of the Class diagram shown in the image link location below:
// https://www.ntu.edu.sg/home/ehchua/programming/java/images/ExerciseOOP_ShapeAndSubclasses.png
// This function should contain test code that demonstrates that you have fully implemented these structures and that the inheritance demonstrated works ﬂawlessly.
//----------------------------------- Function 10 -----------------------------------
"\n\nFunction 10" println
"-----------" println
Shape := Object clone
Shape color := "red"
Shape filled := true
Shape description := "I am a Shape"
Shape express := method(description println)

Circle := Shape clone
Circle description := "I am a Circle and a Shape"
Circle radius := 1.0
Circle area := method (radius, area := 3.14159265359 *  radius *  radius)
Circle perimeter := method(radius, perimeter := 3.14159265359 * 2 *  radius)


Rectangle := Shape clone
Rectangle description := "I am a Rectangle and a Shape "
Rectangle width := 1.0
Rectangle length := 1.0
Rectangle area := method(length, width, area := length * width)
Rectangle perimeter := method(length, width, perimeter := 2.0 * (length + width))


Square := Rectangle clone
Square description := "I am a Square, a Rectangle and a Shape"
Square side := 2* Rectangle width
Square area := method(side, area := side * side)
Square perimeter := method(side, perimeter := 4.0 * side)

Shape express 
Circle express 
Rectangle express 
Square express 

writeln("\nThe radius of a Circle is ", Circle radius," \n=> The area of this Circle is ", Circle area(Circle radius), 
	"\n=> The perimeter of this Circle is ", Circle perimeter(Circle radius))
writeln("\nThe length of a Rectangle is ", Rectangle length, " and the width of a Rectangle is ", Rectangle width,"\n=> The area of this Rectangle is ",
	Rectangle area(Rectangle length, Rectangle width), "\n=> The perimeter of this Rectangle is ", Rectangle perimeter(Rectangle length, Rectangle width))
writeln("\nThe side of a Square is ", Square side, "\n=> The area of this Square is ", Square area(Square side), "\n=> The perimeter of this Square is ",
	Square perimeter(Square side))