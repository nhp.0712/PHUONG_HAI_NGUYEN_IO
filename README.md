1. Function#1: Write a function that prints the string “Hello,world.”
2. Function#2: Write a function that accepts 2 parameters: A string and a substring to ﬁnd in that string. 
    The function should ﬁnd the index of substring in string and print that index.
3. Function#3: Write a function that accepts a single parameter – A list/array – and performs 3 actions: (1) Sort the array and print 
    the values, (2) Sort the array in reverse order and print the values, and (3) Prints the value of each array/list item that has a 
    ODD index. The array passed into this function must contain at least 100 randomly generated values. 
    Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) asyou ﬁnd appropriate.
4. Function#4: Generate a list/array of 100 random values. Sort the list using a SelectionSort
5. Function#5: Implement a function to calculate the Fibonacci Numbers iteratively. Attemptto calculate as many Fibonacci numbers as possible. 
    The Fibonacci Series is deﬁned below with F1=F2=1 and F0=0. Fn = Fn−1+Fn−2 
6. Function#6: Implement a function to calculate the Fibonacci series recursively using Guards and PatternMatching. Compare the
    computation time and lines of code with Function#5.
7. Function#7: Implement a function to perform matrix multiplication. The function should take two parameters – Matrix A and Matrix B – and
    return the resultant matrix – Matrix C. Test by generating a variety of random matrices that are ﬁlled with random decimal values between 
    0 and 100. These ﬁles may be in separate ﬁles, classes, etc. and should follow the paradigms of the current language. 
8. Function#8: The Hamming distance between two numbers is the number of bits that must be changed to make the representations of the two numbers 
    equal. For instance, the word “Rob” and “Bob” differ by the ﬁrst letter, so their Hamming distance is 1. Strings like “Rob” and “Robby” differ
    by 2 characters, so the Hamming distance is 2. Write a function that accepts one parameter – a ﬁlename. This ﬁle will contain two strings on 
    each line, with each string being separated by a “ ”. Your function should read each line, calculate the Hamming distance between the two values, 
    and then write the line back to a new ﬁle in the format Value1:Value2:HammingDistance. It is up to you to generate a test ﬁle for this function. 
    The output ﬁle should insert the string _Output into the original ﬁlename. If the input ﬁle is “HammingData.csv” then the output ﬁle would be HammingData_Output.csv.
9. Function #9: Create a csvReader class that is capable of opening, reading, and accessing the data from any given CSV ﬁle. The csvReader class should use a csVRow object to
    store the data from each row in the CSV ﬁle. This function should contain code that demonstrates the ability of your class to read, iterate, and display the contents of a CSV ﬁle.
10. Function #10: Implement the equivalent of the Class diagram shown in the image link location below:
    https://www.ntu.edu.sg/home/ehchua/programming/java/images/ExerciseOOP_ShapeAndSubclasses.png
    This function should contain test code that demonstrates that you have fully implemented these structures and that the inheritance demonstrated works ﬂawlessly.


-----------------------------------------------------
Instructions for compiling and running this IO file
-----------------------------------------------------

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:
1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_IO
. Compile and run by command: io index.io
-> the solutions for all functions will show up as the example below

2. Using Command Prompt for Windows system
. Log in to the directory by command that contains files: cd /d YOURDIRECTORY (Ex: cd \d C:\Windows)
. Compile and run by command: io index.io
-> the solutions for all functions will show up as the example below


-----------------------------------------------------
        Example Solutions for all functions
-----------------------------------------------------
Io Language − Phuong Nguyen

Function 1
----------
Hello, world.

Function 2
----------
Finding ''Me'' in ''SearchMe''
SearchMe contains Me
Me is found at index 6

Finding ''They'' in ''SearchMe''
SearchMe does not contains They

Function 3
----------
Sorted: 9, 10, 10, 17, 18, 19, 20, 24, 24, 26, 26, 33, 33, 38, 41, 46, 46, 50, 52, 56, 60, 61, 65, 65, 67, 68, 70, 71, 84, 85, 88, 88, 88, 91, 94, 98, 99, 109, 110, 111, 115, 123, 128, 128, 136, 138, 138, 146, 147, 154, 157, 158, 159, 163, 165, 170, 170, 172, 175, 185, 186, 189, 193, 196, 198, 199, 205, 209, 211, 214, 219, 219, 219, 220, 228, 228, 234, 235, 235, 238, 240, 240, 244, 244, 254, 257, 257, 260, 261, 262, 264, 265, 266, 266, 267, 273, 279, 282, 288, 293

Reverse Sorted: 293, 288, 282, 279, 273, 267, 266, 266, 265, 264, 262, 261, 260, 257, 257, 254, 244, 244, 240, 240, 238, 235, 235, 234, 228, 228, 220, 219, 219, 219, 214, 211, 209, 205, 199, 198, 196, 193, 189, 186, 185, 175, 172, 170, 170, 165, 163, 159, 158, 157, 154, 147, 146, 138, 138, 136, 128, 128, 123, 115, 111, 110, 109, 99, 98, 94, 91, 88, 88, 88, 85, 84, 71, 70, 68, 67, 65, 65, 61, 60, 56, 52, 50, 46, 46, 41, 38, 33, 33, 26, 26, 24, 24, 20, 19, 18, 17, 10, 10, 9

Odd only: 10, 17, 19, 24, 26, 33, 38, 46, 50, 56, 61, 65, 68, 71, 85, 88, 91, 98, 109, 111, 123, 128, 138, 146, 154, 158, 163, 170, 172, 185, 189, 196, 199, 209, 214, 219, 220, 228, 235, 238, 240, 244, 257, 260, 262, 265, 266, 273, 282, 293

Function 4
----------
Unsorted: 213, 243, 141, 258, 18, 287, 249, 126, 221, 264, 123, 185, 63, 283, 287, 250, 144, 148, 78, 264, 100, 274, 266, 23, 22, 66, 48, 51, 216, 88, 207, 236, 248, 185, 173, 185, 1, 42, 146, 260, 244, 71, 183, 218, 168, 246, 222, 19, 129, 272, 138, 265, 277, 127, 239, 152, 295, 273, 187, 31, 130, 118, 90, 79, 135, 226, 118, 222, 71, 21, 54, 81, 254, 215, 79, 188, 115, 231, 227, 121, 166, 280, 140, 246, 48, 128, 89, 61, 185, 150, 230, 221, 232, 69, 221, 38, 158, 66, 286, 209

Selection Sort: 1, 18, 19, 21, 22, 23, 31, 38, 42, 48, 48, 51, 54, 61, 63, 66, 66, 69, 71, 71, 78, 79, 79, 81, 88, 89, 90, 100, 115, 118, 118, 121, 123, 126, 127, 128, 129, 130, 135, 138, 140, 141, 144, 146, 148, 150, 152, 158, 166, 168, 173, 183, 185, 185, 185, 185, 187, 188, 207, 209, 213, 215, 216, 218, 221, 221, 221, 222, 222, 226, 227, 230, 231, 232, 236, 239, 243, 244, 246, 246, 248, 249, 250, 254, 258, 260, 264, 264, 265, 266, 272, 273, 274, 277, 280, 283, 286, 287, 287, 295

Function 5
----------
Fib(1) := 1
Fib(2) := 1
Fib(3) := 2
Fib(4) := 3
Fib(5) := 5
Fib(6) := 8
Fib(7) := 13
Fib(8) := 21
Fib(9) := 34
Fib(10) := 55
Fib(11) := 89
Fib(12) := 144
Fib(13) := 233
Fib(14) := 377
Fib(15) := 610
Fib(16) := 987
Fib(17) := 1597
Fib(18) := 2584
Fib(19) := 4181
Fib(20) := 6765

Function 6
----------
Fib(1) := 1
Fib(2) := 1
Fib(3) := 2
Fib(4) := 3
Fib(5) := 5
Fib(6) := 8
Fib(7) := 13
Fib(8) := 21
Fib(9) := 34
Fib(10) := 55
Fib(11) := 89
Fib(12) := 144
Fib(13) := 233
Fib(14) := 377
Fib(15) := 610
Fib(16) := 987
Fib(17) := 1597
Fib(18) := 2584
Fib(19) := 4181
Fib(20) := 6765

Function 7
----------
Matrix A: 
225 280 193 202 212 
3 20 208 258 135 
148 278 79 112 258 
27 31 107 142 257 
62 275 281 37 164 

Matrix B: 
274 257 187 25 174 
149 140 2 233 221 
291 138 299 218 170 
246 232 254 260 8 
147 81 53 231 220 

Matrix C := Matrix A * Matrix B
240389 187695 162886 214431 182096 
147643 103066 135480 148344 72066 
170441 134740 93975 174414 158276 
115865 79806 86793 127511 87415 
172944 115080 114253 174387 155709 

Function 8
----------
CS3060:CS3140:2
Ruby:HTML:4
IOLang:CSSadvance:5

Function 9
----------
Header: Item         Cost         Sold         Profit         
Row 1:  Keyboad      $10.00       $16.00       $6.00       
Row 2:  Monitor      $40.00       $80.00       $40.00       
Row 3:  MousePC      $10.00       $22.00       $12.00       
Row 4:  LenovoX      $90.00       $99.00       $9.00       
Row 5:  Macbook      $50.00       $99.00       $49.00       

Function 10
-----------
I am a Shape
I am a Circle and a Shape
I am a Rectangle and a Shape 
I am a Square, a Rectangle and a Shape

The radius of a Circle is 1 
=> The area of this Circle is 3.1415926535900001
=> The perimeter of this Circle is 6.2831853071800001

The length of a Rectangle is 1 and the width of a Rectangle is 1
=> The area of this Rectangle is 1
=> The perimeter of this Rectangle is 4

The side of a Square is 2
=> The area of this Square is 4
=> The perimeter of this Square is 8
[Finished in 0.3s]